package model.utils;

import java.util.List;

/**
 * This is the interface contract of a boat entity.
 * 
 * @param <E>
 *                   In our purpose the Player class.
 */

public interface Boat<E> extends List<E> {

}
